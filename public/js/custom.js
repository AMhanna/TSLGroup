$(function () {

    // Init date pickers
    initDatePicker();

    // Show flash messages
    showFlashMessages();
});

/**
 * Init date pickers
 */
function initDatePicker() {
    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'DD.MM.YYYY',
        clearButton: true,
        weekStart: 1,
        time: false,
        lang: 'de'
    });
}


    function showFlashMessages() {
        var $flashMessages = $('.flash-messages');
        $flashMessages.find('.flash-message-success').each(function () {
            var message = $(this).text();
            showFlashMessage(message, 'success', 'check');
        });

        $flashMessages.find('.flash-message-notice').each(function () {
            var message = $(this).text();
            showFlashMessage(message, 'warning', 'info');
        });

        $flashMessages.find('.flash-message-error').each(function () {
            var message = $(this).text();
            showFlashMessage(message, 'danger', 'warning');
        });
    }

    function showFlashMessage(message, type, icon) {
        $.notify({
            icon: "fa fa-" + icon,
            message: message
        },{
            type: type,
            offset: 20,
            delay: 1000,
            timer: 1500,
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated flipOutX'
            },
            template: '<div data-notify="container" class="col-xs-10 col-sm-2 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>'
        });
}