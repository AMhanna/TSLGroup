<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180808003554 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user CHANGE certificates certificates LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', CHANGE syllabus syllabus LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', CHANGE facebook facebook VARCHAR(255) DEFAULT NULL, CHANGE twitter twitter VARCHAR(255) DEFAULT NULL, CHANGE linked_in linked_in VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user CHANGE certificates certificates LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:array)\', CHANGE syllabus syllabus LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:array)\', CHANGE facebook facebook VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE twitter twitter VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE linked_in linked_in VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
