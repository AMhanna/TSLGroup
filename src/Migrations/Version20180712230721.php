<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180712230721 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE uploaded_document (id INT AUTO_INCREMENT NOT NULL, uploaded_by INT DEFAULT NULL, file_name VARCHAR(255) DEFAULT NULL, uploaded_at DATETIME NOT NULL, INDEX IDX_4A0FA6E7E3E73126 (uploaded_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, position VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_uploaded_files (asset_id INT NOT NULL, uploaded_document_id INT NOT NULL, INDEX IDX_D19A7D4B5DA1941 (asset_id), UNIQUE INDEX UNIQ_D19A7D4BA20E05A1 (uploaded_document_id), PRIMARY KEY(asset_id, uploaded_document_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE uploaded_document ADD CONSTRAINT FK_4A0FA6E7E3E73126 FOREIGN KEY (uploaded_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE app_uploaded_files ADD CONSTRAINT FK_D19A7D4B5DA1941 FOREIGN KEY (asset_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_uploaded_files ADD CONSTRAINT FK_D19A7D4BA20E05A1 FOREIGN KEY (uploaded_document_id) REFERENCES uploaded_document (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_uploaded_files DROP FOREIGN KEY FK_D19A7D4BA20E05A1');
        $this->addSql('ALTER TABLE app_uploaded_files DROP FOREIGN KEY FK_D19A7D4B5DA1941');
        $this->addSql('DROP TABLE uploaded_document');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE app_uploaded_files');
    }
}
