<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180810013320 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_uploaded_files DROP FOREIGN KEY FK_D19A7D4B5DA1941');
        $this->addSql('DROP TABLE app_uploaded_files');
        $this->addSql('DROP TABLE media');
        $this->addSql('ALTER TABLE contact CHANGE message message LONGTEXT NOT NULL, CHANGE answer answer LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE app_uploaded_files (asset_id INT NOT NULL, uploaded_document_id INT NOT NULL, UNIQUE INDEX UNIQ_D19A7D4BA20E05A1 (uploaded_document_id), INDEX IDX_D19A7D4B5DA1941 (asset_id), PRIMARY KEY(asset_id, uploaded_document_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, description VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, position VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, is_active TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_uploaded_files ADD CONSTRAINT FK_D19A7D4B5DA1941 FOREIGN KEY (asset_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_uploaded_files ADD CONSTRAINT FK_D19A7D4BA20E05A1 FOREIGN KEY (uploaded_document_id) REFERENCES uploaded_document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact CHANGE message message VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE answer answer VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
