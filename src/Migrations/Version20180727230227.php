<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180727230227 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_courses (id INT AUTO_INCREMENT NOT NULL, user INT DEFAULT NULL, course INT DEFAULT NULL, registered_at DATETIME NOT NULL, user_status VARCHAR(255) NOT NULL, course_status VARCHAR(255) NOT NULL, INDEX IDX_F1A844468D93D649 (user), INDEX IDX_F1A84446169E6FB9 (course), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_courses ADD CONSTRAINT FK_F1A844468D93D649 FOREIGN KEY (user) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE user_courses ADD CONSTRAINT FK_F1A84446169E6FB9 FOREIGN KEY (course) REFERENCES course (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user_courses');
    }
}
