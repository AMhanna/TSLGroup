<?php
/**
 * Created by PhpStorm.
 * User: amhanna
 * Date: 07.08.18
 * Time: 02:06
 */

namespace App\Service;


use App\Entity\Company\Company;
use Doctrine\ORM\EntityManagerInterface;

class CompanyHelper
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

    }


    public function getCompanyData()
    {
        $companies = $this->em->getRepository(Company::class)->findAll();

        foreach($companies as $company )
        {
            $companies[] = $company->getId();
        }

        $companyId = '';
        if ($companies)
        {
            $companyId = $companies[array_rand($companies, 1)];
        }
        $company = $this->em->getRepository(Company::class)->find($companyId);

        return $company;
    }
}