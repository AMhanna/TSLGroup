<?php
/**
 * Created by PhpStorm.
 * User: amhanna
 * Date: 07.08.18
 * Time: 00:19
 */

namespace App\DataFixtures;

use App\Entity\Company\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;



class CompanyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

            $company = new Company();
            $company->setName('T.S.L Group')
                    ->setCity('Rosenheim')
                    ->setStreet('Frühlingstr')
                    ->setHouseNumber('39')
                    ->setPlz('83022')
                    ->setFacebook('facebook')
                    ->setInstagram('Instagram')
                    ->setTwitter('twitter')
                    ->setPhoneNumber('+491626905612')
                    ->setEmail('a.mhanna@outlook.com');

            $manager->persist($company);

        $manager->flush();

    }
}