<?php

namespace App\Controller;


use App\Entity\Course\Course;
use App\Entity\Course\UserCourses;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CoursesController extends Controller
{
    /**
     * @Route("/courses",name="courses_index")
     * @Template("courses/index.html.twig")
     *
     * @return array
     */
     public function index()
     {
         return [
             'courses' => $this->getDoctrine()->getRepository(Course::class)->findBy(['status'=>Course::ACTIVE]),
         ];
     }

    /**
     * @Route("/course/show/{id}",name="course_show")
     * @Template("courses/show.html.twig")
     *
     * @param Course $course
     * @return array
     */
     public function show(Course $course)
     {
         return [
             'courseStudents'=>$this->getDoctrine()->getRepository(UserCourses::class)->findBy(['course'=>$course]),
             'course'=>$course
         ];
     }
}