<?php

namespace App\Controller;


use App\Entity\Contact;
use App\Entity\Course\Course;
use App\Entity\Course\UserCourses;
use App\Entity\FeedBacks;
use App\Entity\Slider;
use App\Entity\User\User;
use App\Entity\WelcomeMessage;
use App\Entity\WhoWeAre;
use App\Form\Contact\ContactType;
use App\Service\CompanyHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/",name="homepage")
     * @Template("homepage/index.html.twig")
     *
     * @return array
     */
    public function index()
    {
        $welcomeMessages = $this->getDoctrine()->getRepository(WelcomeMessage::class)->findAll();
        $messagesIds = [];
        foreach($welcomeMessages as $welcomeMessage )
        {
            $messagesIds[] = $welcomeMessage->getId();
        }
        $messageId = '';
        if ($messagesIds){
        $messageId = $messagesIds[array_rand($messagesIds, 1)];
        }
        $welcomeMessage = $this->getDoctrine()->getRepository(WelcomeMessage::class)->find($messageId);

        return [
            'welcomeMessage' => $welcomeMessage,
            'courses'        => $this->getDoctrine()->getRepository(Course::class)->findBy(['status'=>Course::ACTIVE]),
            'sliders'        => $this->getDoctrine()->getRepository(Slider::class)->findAll(),
        ]  ;
    }

    /**
     * @Route("/course-register/{courseId}",name="course_register")
     * @param $courseId
     * @param TranslatorInterface $translator
     * @param \Swift_Mailer $mailer
     * @return  array | RedirectResponse
     */
    public function Registration($courseId,TranslatorInterface $translator,\Swift_Mailer $mailer,Request $request)
    {

        if(!$this->getUser())
        {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $userInTheCourse = $this->getDoctrine()->getRepository(UserCourses::class)->findBy(['course'=>$courseId,'courseMembers'=>$this->getUser()]);

        if(empty($userInTheCourse) )
        {
            $course = $this->getDoctrine()->getRepository(Course::class)->find($courseId);

            $course->setBookedSeats($course->getBookedSeats() + 1 );
            $course->setSeats($course->getSeats() - 1 );

            $certificateData = $request->request->get('certificate');
            $userCourses = new UserCourses();
            $userCourses->setCourse($course)
                        ->setCourseMembers($this->getUser())
                        ->setRegisteredAt(new \DateTime())
                        ->setUserStatus(UserCourses::APPLIED)
                        ->setCourseStatus(UserCourses::AVAILABLE);
            if($certificateData)
            {
                $userCourses->setWithCertificate(true);
            }
            $this->getDoctrine()->getManager()->persist($userCourses);
            $this->getDoctrine()->getManager()->flush();



            $html = $this->renderView('contract.pdf.html.twig',
                [
                    'certificateData'=> $certificateData,
                    'course'         => $course,
                    'user'           => $this->getUser()
                ]
            );
            $this->get('knp_snappy.pdf')->generateFromHtml($html,'../contracts/'.$this->getUser()->getLastName().$courseId.'.pdf' );

            $message = (new \Swift_Message('Course Registration Contract'))
                ->setFrom('support@tsl-group.org')
                ->setTo($this->getUser()->getEmail())
                ->setBody(
                    $this->renderView(
                        'courses/registration.email.html.twig',
                       [
                           'user' => $this->getUser(),
                           'course'  => $course,
                       ]
                    ),
                    'text/html'
                )
                ->attach(\Swift_Attachment::fromPath('../contracts/'.$this->getUser()->getLastName().$courseId.'.pdf'))
                ->attach(\Swift_Attachment::fromPath('../contracts/Widerrufsbelehrung.docx'))
            ;

            $mailer->send($message);

            $this->addFlash('success',$translator->trans('course.registeration.success'));
            return $this->redirectToRoute('course_show',['id'=>$courseId]);
        }
        $this->addFlash('error',$translator->trans('course.registeration.error'));
        return $this->redirectToRoute('course_show',['id'=>$courseId]);
    }

    /**
     * @Route("about",name="about_us")
     * @Template("about.html.twig")
     *
     * @return array
     */
    public function AboutUs()
    {
        $whoWeAreMessages = $this->getDoctrine()->getRepository(WhoWeAre::class)->findAll();
        $messagesIds = [];
        foreach($whoWeAreMessages as $whoWeAreMessage )
        {
            $messagesIds[] = $whoWeAreMessage->getId();
        }
        $messageId = '';
        if ($messagesIds){
            $messageId = $messagesIds[array_rand($messagesIds, 1)];
        }

        $whoWeAreMessage = $this->getDoctrine()->getRepository(WhoWeAre::class)->find($messageId);

        return
            [
                'whoWeAre' => $whoWeAreMessage,
                'feedBacks'=> $this->getDoctrine()->getRepository(FeedBacks::class)->findAll(),
                'teachers' => $this->getDoctrine()->getRepository(User::class)->findBy(['isTeacher' => true ]),
                'users'    => $this->getDoctrine()->getRepository(User::class)->findAll(),
                'courses'    => $this->getDoctrine()->getRepository(Course::class)->findAll(),
            ];
    }

    /**
     * @Route("company-training",name="company_training")
     * @Template("companyTraining.html.twig")
     *
     * @return array
     */
    public function CorporateTraining()
    {
        return [];
    }

    /**
     * @Route("research",name="research")
     * @Template("research.html.twig")
     *
     * @return array
     */
    public function Research()
    {
        return [];
    }

    /**
     * @Route("contact",name="contact")
     * @Template("contact.html.twig")
     *
     * @return array
     */
    public function Contact(Request $request, TranslatorInterface $translator, CompanyHelper $companyHelper)
    {
        $contact = new Contact();
        $form =  $this->createForm(ContactType::class,$contact);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid())
        {

            $this->getDoctrine()->getManager()->persist($contact);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success',$translator->trans('app.contact.success.message'));
        }
        return
            [
                'form'=>$form->createView(),
            ];
    }
}