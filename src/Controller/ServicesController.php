<?php
/**
 * Created by PhpStorm.
 * User: amhanna
 * Date: 10.08.18
 * Time: 22:22
 */

namespace App\Controller;


use App\Entity\Services;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ServicesController extends Controller
{
    /**
     * @Route("/services",name="services")
     * @Template("services/index.htm.twig")
     *
     * @return array
     */
    public function index()
    {
        return
            [
            'services'=>$this->getDoctrine()->getRepository(Services::class)->findAll(),
            ];
    }

    /**
     * @Route("/service/show/{id}" ,name="service_show")
     * @Template("services/show.html.twig")
     *
     * @return array
     */
    public function show()
    {
        return [];
    }
}