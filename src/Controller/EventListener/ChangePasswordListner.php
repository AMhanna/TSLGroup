<?php
/**
 * Created by PhpStorm.
 * User: amhanna
 * Date: 03.08.18
 * Time: 22:31
 */

namespace App\Controller\EventListener;


use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;

class ChangePasswordListner implements EventSubscriberInterface
{
    private $router;
    private $message;
    private $translator;

    public function __construct(UrlGeneratorInterface $router,FlashBagInterface $message,TranslatorInterface $translator)
    {
        $this->router = $router;
        $this->message = $message;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::CHANGE_PASSWORD_SUCCESS => 'onChangePasswordSuccess',
            FOSUserEvents::RESETTING_RESET_SUCCESS => 'onResetPasswordSuccess'
        );
    }

    public function onChangePasswordSuccess(FormEvent $event)
    {

        $url = $this->router->generate('fos_user_change_password');
        $event->setResponse(new RedirectResponse($url));
    }

    public function onResetPasswordSuccess(FormEvent $event)
    {
        $url = $this->router->generate('fos_user_profile_edit');
        $event->setResponse(new RedirectResponse($url));
    }
}