<?php

namespace App\Controller\EventListener;


use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;

class ProfileUpdateListener implements EventSubscriberInterface
{
    private $router;
    private $message;
    private $translator;


    public function __construct(UrlGeneratorInterface $router,FlashBagInterface $message,TranslatorInterface $translator)
    {
        $this->router = $router;
        $this->message = $message;
        $this->translator = $translator;

    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::PROFILE_EDIT_SUCCESS => 'onProfileUpdatingSuccess',
        );
    }

    public function onProfileUpdatingSuccess(FormEvent $event)
    {

        $url = $this->router->generate('fos_user_profile_edit');
        $event->setResponse(new RedirectResponse($url));
    }
}