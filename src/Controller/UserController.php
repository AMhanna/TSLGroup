<?php

namespace App\Controller;


use App\Form\User\BankType;
use App\Form\User\TeacherType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

class UserController extends Controller
{
    /**
     * @Route("/profile/edit/bank-data",name="user_bank_data")
     * @Template("user/user.bank.html.twig")
     *
     * @param Request $request
     * @return array
     */
    public function addBankData(Request $request,TranslatorInterface $translator)
    {
        $user = $this->getUser();
        $form = $this->createForm(BankType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success',$translator->trans('app.success.message'));
        }
        return
            [
                'form'=>$form->createView()
            ];
    }

    /**
     * @Route("/profile/edit/teacher-data",name="user_teacher_data")
     * @Template("user/user.teacher.html.twig")
     *
     * @param Request $request
     * @return array
     */
    public function addTeacherData(Request $request,TranslatorInterface $translator)
    {
        $user = $this->getUser();
        $form = $this->createForm(TeacherType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success',$translator->trans('app.success.message'));
        }
        return
            [
                'form'=>$form->createView()
            ];
    }
}