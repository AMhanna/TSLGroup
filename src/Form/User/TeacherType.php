<?php
/**
 * Created by PhpStorm.
 * User: amhanna
 * Date: 08.08.18
 * Time: 00:11
 */

namespace App\Form\User;


use App\Entity\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TeacherType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

      $builder->add(
          'facebook',
          TextType::class,
          [
              'label'=>'form.facebook',
              'attr'=>
                          [
                              'class'=>'form-control',
                          ]
          ]);

      $builder->add(
          'linkedIn',
          TextType::class,
          [
              'label'=>'form.linkedIn',
              'attr'=>
                  [
                      'class'=>'form-control',
                  ]
          ]);

      $builder->add(
          'twitter',
          TextType::class,
          [
              'label'=>'form.twitter',
              'attr'=>
                  [
                      'class'=>'form-control',
                  ]
          ]);
      $builder->add(
          'certificates',
          CollectionType::class,
          [
              'entry_type' => TextType::class,
              'label' => false,
              'allow_add'=> true,
              'allow_delete'=> true,
//              'entry_options'=>
//                  [
//                      'attr'=>
//                          [
//                              'class'=>'form-control',
//                          ]
//                  ]
          ]
      );

      $builder->add(
          'syllabus',
          CollectionType::class,
          [
              'entry_type' => TextType::class,
              'label' => false,
              'allow_add'=> true,
              'allow_delete'=> true,
//              'entry_options'=>
//                  [
//                      'attr'=>
//                          [
//                              'class'=>'form-control',
//                          ]
//                  ]
          ]
      );
  }

    public function getBlockPrefix()
    {
        return 'app_user_teacher';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

}