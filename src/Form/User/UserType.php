<?php

namespace App\Form\User;


use App\Entity\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'email',
            EmailType::class,
            [
                'label' => 'form.email',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.email',
                    ],

            ]
        );
        $builder->add(
            'username',
            TextType::class,
            [
                'label' => false,
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.username',
                    ]
            ]
        );

        $builder->add(
            'firstname',
            TextType::class,
            [
                'label' => 'form.firstname',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.firstname',
                    ]
            ]
        );

        $builder->add(
            'lastname',
            TextType::class,
            [
                'label' => 'form.lastname',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.lastname'
                    ]
            ]
        );

        $builder->add(
            'street',
            TextType::class,
            [
                'label' => 'form.street',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.street'
                    ]
            ]
        );

        $builder->add(
            'houseNumber',
            NumberType::class,
            [
                'label' => 'form.houseNumber',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.houseNumber'
                    ]
            ]
        );

        $builder->add(
            'plz',
            NumberType::class,
            [
                'label' => 'form.plz',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.plz'
                    ]
            ]
        );

        $builder->add(
            'city',
            TextType::class,
            [
                'label' => 'form.city',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.city'
                    ]
            ]
        );

        $builder->add(
            'phoneNumber',
            TelType::class,
            [
                'label' => 'form.phoneNumber',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.phoneNumber'
                    ]
            ]
        );

        $builder->add(
            'birthDay',
            DateType::class,
            [
                'label' => 'form.birthday',
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
                'attr'=>
                    [
                        'class'=>'form-control datepicker-here',
                        'placeholder'=>'form.birthday',
                        'data-language'=>'en',
                        'data-date-format'=>'dd.mm.yyyy'
                    ],
            ]
        );

        $builder->add(
            'gender',
            ChoiceType::class,
            [
                'label' => 'form.gender',
                'choices'=>[
                    'form.male'=>'male',
                    'form.female'=>'female',
                ],
                'attr'=>
                    [
                        'class'=>'form-control',
                    ],
            ]
        );

        $builder->add('imageFile', VichImageType::class,
            [
                'label'=>'Upload Your Image',
                'required' => false,
                'allow_delete' => false,
                'download_label' => false,
                'download_uri' => false,
                'image_uri' => true,
                'attr'      =>
                    [
                        'class' => 'btn btn-success',
                    ],
            ]);

        $builder  ->add('plainPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'label' => false,
            'options' =>[
                'attr' => [
                    'autocomplete' => 'new-password',
                    'class'=>'form-control',
                    'placeholder'=>'form.password'
                ],
            ],
            'first_options' => ['label' => false,],
            'second_options' =>
                [
                    'label' => false,
                    'attr' =>
                        [
                            'placeholder'=>'form.password_confirmation',
                            'class'=>'form-control',
                        ]
                ],
            'invalid_message' => 'fos_user.password.mismatch',

        ]);

        $builder->add('imageFile', VichImageType::class,
            [
                'label'=>'Profile Image',
                'required' => false,
                'allow_delete' => false,
                'download_label' => false,
                'download_uri' => false,
                'image_uri' => true,
                'attr'      =>
                    [
                        'class' => 'btn btn-warning',
                    ],
            ]);
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}