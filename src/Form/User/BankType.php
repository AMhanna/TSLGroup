<?php

namespace App\Form\User;


use App\Entity\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BankType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'accountOwner',
            TextType::class,
            [
                'label' => 'form.accountOwner',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.accountOwner',
                    ],

            ]
        );

        $builder->add(
            'bankName',
            TextType::class,
            [
                'label' => 'form.bankName',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.bankName',
                    ],

            ]
        );

        $builder->add(
            'bic',
            TextType::class,
            [
                'label' => 'form.bic',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.bic',
                    ],

            ]
        );

        $builder->add(
            'iban',
            TextType::class,
            [
                'label' => 'form.iban',
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.iban',
                    ],

            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'app_user_bank';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}