<?php
/**
 * Created by PhpStorm.
 * User: amhanna
 * Date: 06.08.18
 * Time: 12:23
 */

namespace App\Form\Contact;


use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            [
                'label' => false,
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.name',
                    ],

            ]
        );

        $builder->add(
            'email',
            EmailType::class,
            [
                'label' => false,
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.email',
                    ],

            ]
        );

        $builder->add(
            'phone',
            NumberType::class,
            [
                'label' => false,
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.phoneNumber',
                    ],

            ]
        );

        $builder->add(
            'subject',
            TextType::class,
            [
                'label' => false,
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.subject',
                    ],

            ]
        );

        $builder->add(
            'message',
            TextareaType::class,
            [
                'label' => false,
                'attr'=>
                    [
                        'class'=>'form-control',
                        'placeholder'=>'form.message',
                    ],

            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'contact';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}