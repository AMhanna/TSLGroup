<?php
/**
 * Created by PhpStorm.
 * User: amhanna
 * Date: 05.08.18
 * Time: 22:37
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity
 * @ORM\Table(name="welcome_message")
 * @Vich\Uploadable
 */
class WelcomeMessage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string",length=50)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle():? string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return WelcomeMessage
     */
    public function setTitle(string $title): WelcomeMessage
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage():? string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return WelcomeMessage
     */
    public function setMessage(string $message): WelcomeMessage
    {
        $this->message = $message;
        return $this;
    }

}