<?php
/**
 * Created by PhpStorm.
 * User: amhanna
 * Date: 05.08.18
 * Time: 23:54
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="feed_backs")
 */
class FeedBacks
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string",length=155)
     */
    private $studentName;


    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStudentName():? string
    {
        return $this->studentName;
    }

    /**
     * @param string $studentName
     * @return feedBacks
     */
    public function setStudentName(string $studentName): feedBacks
    {
        $this->studentName = $studentName;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage():? string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return feedBacks
     */
    public function setMessage(string $message): feedBacks
    {
        $this->message = $message;
        return $this;
    }


}