<?php
/**
 * Created by PhpStorm.
 * User: amhanna
 * Date: 06.08.18
 * Time: 12:12
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="contact")
 */
class Contact
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\Email()
     */
    private $email;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     *
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $message;

    /**
     * @var boolean
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isAnswered;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $answer;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName():? string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Contact
     */
    public function setName(string $name): Contact
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail():? string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Contact
     */
    public function setEmail(string $email): Contact
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return int
     */
    public function getPhone():? int
    {
        return $this->phone;
    }

    /**
     * @param int $phone
     * @return Contact
     */
    public function setPhone(int $phone): Contact
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject():? string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return Contact
     */
    public function setSubject(string $subject): Contact
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage():? string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Contact
     */
    public function setMessage(string $message): Contact
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAnswered():? bool
    {
        return $this->isAnswered;
    }

    /**
     * @param bool $isAnswered
     * @return Contact
     */
    public function setIsAnswered(bool $isAnswered): Contact
    {
        $this->isAnswered = $isAnswered;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer():? string
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     * @return Contact
     */
    public function setAnswer(string $answer): Contact
    {
        $this->answer = $answer;
        return $this;
    }


}