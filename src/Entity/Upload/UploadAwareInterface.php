<?php

namespace App\Entity\Upload;

use Doctrine\Common\Collections\ArrayCollection;

interface UploadAwareInterface
{
    /**
     * Get all uploadedDocuments
     *
     * @return ArrayCollection<UploadedDocument>
     */
    public function getUploadedDocuments();

    /**
     * Add an uploadedDocument
     *
     * @param UploadedDocument $uploadedDocument
     *
     * @return UploadAwareInterface
     */
    public function addUploadedDocument(UploadedDocument $uploadedDocument);

    /**
     * Remove an uploadedDocument
     *
     * @param UploadedDocument $uploadedDocument
     *
     * @return UploadAwareInterface
     */
    public function removeUploadedDocument(UploadedDocument $uploadedDocument);
}
