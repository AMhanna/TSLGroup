<?php

namespace App\Entity\Upload;

use Doctrine\Common\Collections\ArrayCollection;

trait Uploadable
{
    /**
     * @var ArrayCollection<UploadedDocument> $uploadedDocuments
     */
    private $uploadedDocuments;

    /**
     * Get all uploadedDocuments
     *
     * @return ArrayCollection<UploadedDocument>
     */
    public function getUploadedDocuments()
    {
        return $this->uploadedDocuments;
    }

    /**
     * Add an uploadedDocument
     *
     * @param UploadedDocument $uploadedDocument
     *
     * @return $this
     */
    public function addUploadedDocument(UploadedDocument $uploadedDocument)
    {
        $this->uploadedDocuments->add($uploadedDocument);

        return $this;
    }

    /**
     * Remove an uploadedDocument
     *
     * @param UploadedDocument $uploadedDocument
     *
     * @return $this
     */
    public function removeUploadedDocument(UploadedDocument $uploadedDocument)
    {
        $this->uploadedDocuments->removeElement($uploadedDocument);

        return $this;
    }

}
