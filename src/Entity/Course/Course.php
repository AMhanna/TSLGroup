<?php

namespace App\Entity\Course;


use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="course")
 */
class Course
{

    public const NEW = 0 ;
    public const PENDING = 10 ;
    public const ACTIVE = 20 ;
    public const FINISHED = 30 ;
    public const DEACTIVE = 40 ;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string $description
     *
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var \DateTime $startsAt
     *
     * @ORM\Column(type="date")
     */
    private $startsAt;

    /**
     * @var  \DateTime $endsAt
     *
     * @ORM\Column(type="date")
     */
    private $endsAt;

    /**
     * @var integer $status
     *
     * @ORM\Column(type="integer")
     */
    private $status = self::ACTIVE;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $cost ;

    /**
     *@var int
     * @ORM\Column(type="integer")
     */
    private $seats;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $bookedSeats;

    /**
     * @var \DateTime $startingHour
     * @ORM\Column(type="time")
     */
    private $startingHour;

    /**
     * @var \DateTime $finishingHour
     * @ORM\Column(type="time")
     */
    private $finishingHour;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $certificateFees;

    /**
     * @var string $courseHours
     * @ORM\Column(type="string")
     */
    private $courseHours;

    /**
     * @var User $teacher
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(name="teacher",referencedColumnName="id",onDelete="SET NULL")
     */
    private $teacher;

    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Course
     */
    public function setTitle( $title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Course
     */
    public function setDescription( $description)
    {
        $this->description = $description;
        return $this;
    }


    /**
     * @return \DateTime
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * @param \DateTime $startsAt
     * @return Course
     */
    public function setStartsAt( $startsAt)
    {
        $this->startsAt = $startsAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }

    /**
     * @param \DateTime $endsAt
     * @return Course
     */
    public function setEndsAt( $endsAt)
    {
        $this->endsAt = $endsAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Course
     */
    public function setStatus( $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param int $cost
     * @return Course
     */
    public function setCost( $cost)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return int
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param int $seats
     * @return Course
     */
    public function setSeats( $seats)
    {
        $this->seats = $seats;
        return $this;
    }

    /**
     * @return int
     */
    public function getBookedSeats()
    {
        return $this->bookedSeats;
    }

    /**
     * @param int $bookedSeats
     * @return Course
     */
    public function setBookedSeats( $bookedSeats)
    {
        $this->bookedSeats = $bookedSeats;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartingHour():? \DateTime
    {
        return $this->startingHour;
    }

    /**
     * @param \DateTime $startingHour
     * @return Course
     */
    public function setStartingHour(\DateTime $startingHour): Course
    {
        $this->startingHour = $startingHour;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFinishingHour():? \DateTime
    {
        return $this->finishingHour;
    }

    /**
     * @param \DateTime $finishingHour
     * @return Course
     */
    public function setFinishingHour(\DateTime $finishingHour): Course
    {
        $this->finishingHour = $finishingHour;
        return $this;
    }


    /**
     * @return User
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @param User $teacher
     * @return Course
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;
        return $this;
    }

    /**
     * @return int
     */
    public function getCertificateFees():? int
    {
        return $this->certificateFees;
    }

    /**
     * @param int $certificateFees
     * @return Course
     */
    public function setCertificateFees(int $certificateFees): Course
    {
        $this->certificateFees = $certificateFees;
        return $this;
    }

    /**
     * @return string
     */
    public function getCourseHours():? string
    {
        return $this->courseHours;
    }

    /**
     * @param string $courseHours
     * @return Course
     */
    public function setCourseHours(string $courseHours): Course
    {
        $this->courseHours = $courseHours;
        return $this;
    }

}