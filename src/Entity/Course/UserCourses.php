<?php

namespace App\Entity\Course;


use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_courses")
 */
class UserCourses
{

    public const APPLIED = 10 ;
    public const PENDING = 20 ;
    public const APPROVED = 30;
    public const AVAILABLE = 10 ;
    public const FULL = 20 ;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var $registeredAt
     *
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    /**
     * @var $userStatus
     *
     * @ORM\Column(type="string")
     */
    private $userStatus;

    /**
     * @var $courseStatus
     *
     * @ORM\Column(type="string")
     */
    private $courseStatus;

    /**
     * @var User $courseMembers
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(name="user",referencedColumnName="id",onDelete="SET NULL")
     *
     */
    private $courseMembers;

    /**
     * @var Course $course
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Course\Course")
     *@ORM\JoinColumn(name="course",referencedColumnName="id",onDelete="SET NULL")
     */
    private $course;

    /**
     * @var boolean
     * @ORM\Column(type="boolean",nullable=true )
     */
    private $withCertificate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getRegisteredAt()
    {
        return $this->registeredAt;
    }

    /**
     * @param mixed $registeredAt
     * @return UserCourses
     */
    public function setRegisteredAt($registeredAt)
    {
        $this->registeredAt = $registeredAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserStatus()
    {
        return $this->userStatus;
    }

    /**
     * @param mixed $userStatus
     * @return UserCourses
     */
    public function setUserStatus($userStatus)
    {
        $this->userStatus = $userStatus;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseStatus()
    {
        return $this->courseStatus;
    }

    /**
     * @param mixed $courseStatus
     * @return UserCourses
     */
    public function setCourseStatus($courseStatus)
    {
        $this->courseStatus = $courseStatus;
        return $this;
    }

    /**
     * @return User
     */
    public function getCourseMembers()
    {
        return $this->courseMembers;
    }

    /**
     * @param User $courseMembers
     * @return UserCourses
     */
    public function setCourseMembers(User $courseMembers)
    {
        $this->courseMembers = $courseMembers;
        return $this;
    }

    /**
     * @return Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param Course $course
     * @return UserCourses
     */
    public function setCourse(Course $course)
    {
        $this->course = $course;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWithCertificate():? bool
    {
        return $this->withCertificate;
    }

    /**
     * @param bool $withCertificate
     * @return UserCourses
     */
    public function setWithCertificate(bool $withCertificate): UserCourses
    {
        $this->withCertificate = $withCertificate;
        return $this;
    }


}