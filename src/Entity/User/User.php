<?php

namespace App\Entity\User;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;



/**
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @var string $firstName
     *
     * @ORM\Column(type="string",length=255)
     *
     * @Assert\NotBlank()
     */
    private $firstName ;

    /**
     * @var string $lastName
     *
     * @ORM\Column(type="string",length=255)
     *
     * @Assert\NotBlank()
     */
    private $lastName ;

    /**
     * @var string $street
     *
     * @ORM\Column(type="string",length=255)
     *
     * @Assert\NotBlank()
     */
    private $street ;

    /**
     * @var integer $houseNumber
     *
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank()
     */
    private $houseNumber ;

    /**
     * @var integer $plz
     *
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank()
     */
    private $plz ;

    /**
     * @var string $city
     *
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank()
     *
     */
    private $city ;

    /**
     * @var string $phoneNumber
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     */
    private $phoneNumber ;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     */
    private $gender;

    /**
     *
     * @ORM\Column(type="date")
     *
     * @Assert\NotBlank()
     *
     */
    private $birthDay;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     *
     *
     */
    private $accountOwner;

    /**
     * @var string
     *@ORM\Column(type="string",nullable=true)
     *
     *
     */
    private $bankName;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     *
     */
    private $iban;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     *
     */
    private $bic;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="profile_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var boolean
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isTeacher;

    /**
     * @var array
     * @ORM\Column(type="array",nullable=true)
     */
    private $certificates;
    /**
     * @var array
     * @ORM\Column(type="array",nullable=true)
     */
    private $syllabus;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     * @Assert\Url()
     */
    private $facebook;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     * @Assert\Url()
     */
    private $twitter;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     * @Assert\Url()
     */
    private $linkedIn;

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return User
     */
    public function setCity( $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return User
     */
    public function setFirstName( $firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return User
     */
    public function setLastName( $lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return User
     */
    public function setStreet( $street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return int
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @param int $houseNumber
     * @return User
     */
    public function setHouseNumber( $houseNumber)
    {
        $this->houseNumber = $houseNumber;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlz()
    {
        return $this->plz;
    }

    /**
     * @param int $plz
     * @return User
     */
    public function setPlz($plz)
    {
        $this->plz = $plz;
        return $this;
    }

    /**
     * @return int
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param int $phoneNumber
     * @return User
     */
    public function setPhoneNumber( $phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }


    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * @param \DateTime $birthDay
     * @return User
     */
    public function setBirthDay( $birthDay)
    {
        $this->birthDay = $birthDay;
        return $this;
    }


    /**
     * @return string
     */
    public function getAccountOwner()
    {
        return $this->accountOwner;
    }

    /**
     * @param string $accountOwner
     * @return User
     */
    public function setAccountOwner( $accountOwner)
    {
        $this->accountOwner = $accountOwner;
        return $this;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     * @return User
     */
    public function setBankName( $bankName)
    {
        $this->bankName = $bankName;
        return $this;
    }

    /**
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @param string $bic
     * @return User
     */
    public function setBic( $bic)
    {
        $this->bic = $bic;
        return $this;
    }

    /**
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     * @return User
     */
    public function setIban( $iban)
    {
        $this->iban = $iban;
        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt():? \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return bool
     */
    public function isTeacher():? bool
    {
        return $this->isTeacher;
    }

    /**
     * @param bool $isTeacher
     * @return User
     */
    public function setIsTeacher(bool $isTeacher):? User
    {
        $this->isTeacher = $isTeacher;
        return $this;
    }

    /**
     * @return array
     */
    public function getCertificates():? array
    {
        return $this->certificates;
    }

    /**
     * @param array $certificates
     * @return User
     */
    public function setCertificates(array $certificates): User
    {
        $this->certificates = $certificates;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebook():? string
    {
        return $this->facebook;
    }

    /**
     * @param string $facebook
     * @return User
     */
    public function setFacebook(string $facebook): User
    {
        $this->facebook = $facebook;
        return $this;
    }

    /**
     * @return string
     */
    public function getTwitter():? string
    {
        return $this->twitter;
    }

    /**
     * @param string $twitter
     * @return User
     */
    public function setTwitter(string $twitter): User
    {
        $this->twitter = $twitter;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkedIn():? string
    {
        return $this->linkedIn;
    }

    /**
     * @param string $linkedIn
     * @return User
     */
    public function setLinkedIn(string $linkedIn): User
    {
        $this->linkedIn = $linkedIn;
        return $this;
    }

    /**
     * @return array
     */
    public function getSyllabus():? array
    {
        return $this->syllabus;
    }

    /**
     * @param array $syllabus
     * @return User
     */
    public function setSyllabus(array $syllabus): User
    {
        $this->syllabus = $syllabus;
        return $this;
    }

    public function hasAllRequiredFieldsFoRegisteration()
    {
        $bankValues = $this->accountOwner && $this->iban &&  $this->bic && $this->bankName;

        return $bankValues ;
    }
}