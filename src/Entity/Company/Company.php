<?php
/**
 * Created by PhpStorm.
 * User: amhanna
 * Date: 07.08.18
 * Time: 00:08
 */

namespace App\Entity\Company;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;


/**
 * @ORM\Entity
 * @ORM\Table(name="company")
 * @Vich\Uploadable
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $facebook;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $twitter;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $instagram;

    /**
     * @var string $street
     *
     * @ORM\Column(type="string",length=255)
     *
     * @Assert\NotBlank()
     */
    private $street ;

    /**
     * @var integer $houseNumber
     *
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank()
     */
    private $houseNumber ;

    /**
     * @var integer $plz
     *
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank()
     */
    private $plz ;

    /**
     * @var string $city
     *
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank()
     *
     */
    private $city ;

    /**
     * @var string $phoneNumber
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     */
    private $phoneNumber ;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="all_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Company
     */
    public function setName(string $name): Company
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail():? string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Company
     */
    public function setEmail(string $email): Company
    {
        $this->email = $email;
        return $this;
    }



    /**
     * @return string
     */
    public function getFacebook(): string
    {
        return $this->facebook;
    }

    /**
     * @param string $facebook
     * @return Company
     */
    public function setFacebook(string $facebook): Company
    {
        $this->facebook = $facebook;
        return $this;
    }

    /**
     * @return string
     */
    public function getTwitter(): string
    {
        return $this->twitter;
    }

    /**
     * @param string $twitter
     * @return Company
     */
    public function setTwitter(string $twitter): Company
    {
        $this->twitter = $twitter;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstagram(): string
    {
        return $this->instagram;
    }

    /**
     * @param string $instagram
     * @return Company
     */
    public function setInstagram(string $instagram): Company
    {
        $this->instagram = $instagram;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return Company
     */
    public function setStreet(string $street): Company
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return int
     */
    public function getHouseNumber(): int
    {
        return $this->houseNumber;
    }

    /**
     * @param int $houseNumber
     * @return Company
     */
    public function setHouseNumber(int $houseNumber): Company
    {
        $this->houseNumber = $houseNumber;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlz(): int
    {
        return $this->plz;
    }

    /**
     * @param int $plz
     * @return Company
     */
    public function setPlz(int $plz): Company
    {
        $this->plz = $plz;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Company
     */
    public function setCity(string $city): Company
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return int
     */
    public function getPhoneNumber(): int
    {
        return $this->phoneNumber;
    }

    /**
     * @param int $phoneNumber
     * @return Company
     */
    public function setPhoneNumber(int $phoneNumber): Company
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt():? \DateTime
    {
        return $this->updatedAt;
    }

}